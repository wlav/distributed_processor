import copy
import numpy as np
import pytest

from common import QubiC_OpenQasmTest, have_prerequisites

if not have_prerequisites:
   pytest.skip("prerequisites not met", allow_module_level=True)


class TestDECLARATIONS(QubiC_OpenQasmTest):
    """Test range of declarations of classical and qubits"""

    options = {
        'barrier': 'none',
        'entangler' : 'cz',
        'h_impgate' : 'X90',
    }

    preamble = """
        OPENQASM 3.0;
        include "stdgates.inc";
    """

    def test01_simple_declarations(self):
        """Simple declarations of qubits and classical registers"""

        prog = self.preamble + """
        bit c0;
        qubit q2;
        c0 = measure q2;
        """

        self._verify2qubic(prog, options=self.options)

    def test02_array_of_one_declarations(self):
        """Array of one declarations of qubits and classical registers"""

        template = self.preamble + """
        bit{0};
        qubit{1};
        h {3};
        {2} = measure {3};
        """

        # arrays of 1 are transpiled as regular variables
        for regs in [(' c0',    '[1] q2', 'c0',    'q2[0]'),
                     ('[1] c0', ' q0',    'c0[0]', 'q0'),
                     ('[1] c0', '[1] q2', 'c0[0]', 'q2[0]'),
                    ]:
            self._verify2qubic(template.format(*regs), options=self.options)

    def test03_regular_array_declarations(self):
        """Array declarations of qubits and classical registers"""

        prog = self.preamble + """
        bit[2] c0;
        qubit[2] q2;
        h q2[0];
        h q2[1];
        c0[0] = measure q2[0];
        c0[1] = measure q2[1];
        """

        qubic_prog = self._verify2qubic(prog, options=self.options)

        # q2[0] and q2[1] should by default be translated into q0 and q1
        for instr in qubic_prog:
            try:
                qubits = instr['qubit']
                if isinstance(qubits, str):
                    qubits = [qubits]

                for q in qubits:
                    assert q in ['Q0', 'Q1']
            except KeyError:
                pass

    def test04_arrays_in_conditions(self):
        """Use of array variables in conditions"""

        template = self.preamble + """
        bit{0};
        qubit{1};
        h {3};
        {2} = measure {3};
        if ({2} == 1) {{ h {3}; }}
        if (1 == {2}) {{ h {3}; }}
        {2} = measure {3};
        """

        # arrays of 1 are transpiled as regular variables (note: for the
        # conditional, Qiskit requires the register to be an array, not
        # a single bit, so the latter case is skipped)
        for regs in [('[1] c0', ' q0',    'c0[0]', 'q0'),
                     ('[1] c0', '[1] q2', 'c0[0]', 'q2[0]'),
                    ]:
            # only translate/assemble ... don't know how to check programs
            # with branching against Qiskit yet
            self._qasm2qubic(template.format(*regs), options=self.options)
